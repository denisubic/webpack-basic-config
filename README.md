# Webpack basic config

Basic configuration for webpack 4. This project is a good way to start with webpack and some dependencies. It uses `browser-sync` instead of `webpack-dev-server` to mount a development server.

## Installation
1. Clone the project
2. Run `npm install -d` to load all dev dependencies
3. Edit the `webpack.config.js` and update the different variables
4. During development, just run `npm run dev` to make `webpack` watch the files and launch the server.
5. In production mode, run `npm run prod` to minimize CSS and JS files.

## Remarks
This project is using a lot of default webpack config like the `output` folder, which is `dist` in this case.

## Customization
Feel free to set your wanted configuration variables, like `output` directory.

## TODO
 - Documentation about babel
 - Enable autoprefixer
 - Comment configuration file