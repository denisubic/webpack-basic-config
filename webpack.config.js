const devMode = process.env.NODE_ENV !== 'production'
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");


let config = {
  devtool: 'cheap-eval-source-map',
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader",
          "import-glob"
        ]
      }, {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: ['url-loader']
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ]
};

if (!devMode) {
  config.plugins.push(new UglifyJsPlugin({
    cache: true,
    parallel: true,
    sourceMap: false
  }));
  config.plugins.push(new OptimizeCSSAssetsPlugin({}));
} else {
  config.plugins.push(new BrowserSyncPlugin({
    host: 'localhost',
    port: 3000,
    proxy: 'http://myapp.test'
  }));
}

module.exports = config;
